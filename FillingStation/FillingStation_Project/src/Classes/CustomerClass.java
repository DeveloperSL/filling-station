/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Kavii
 */
public class CustomerClass {
    private String CustomerNIC;
    private String CustomerName;
    private String CustomerTelephone;
    private String CustomerAddress;
    private double CreditAmount;
    
    public Connection con=null;
    public PreparedStatement pst = null;
    
    
     public void SetCreditCustomer(String pCustomerNIC,String pCustomerName,String pCustomerTelephone,String pCustomerAddress,double pCreditAmount){
         
       this.CustomerNIC=pCustomerNIC;
       this.CustomerName=pCustomerName;
       this.CustomerTelephone=pCustomerTelephone;
       this.CustomerAddress=pCustomerAddress;
       this.CreditAmount=pCreditAmount;
       
       DateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
       Calendar calendar = Calendar.getInstance();
       String PurchaseDate = dateFormat.format(calendar.getTime());
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="INSERT INTO Customer(NIC,Name,Telephone,Address,Credit_Amount,Purchase_Date) VALUES('"+CustomerNIC+"','"+CustomerName+"','"+CustomerTelephone+"','"+CustomerAddress+"','"+CreditAmount+"','"+PurchaseDate+"')";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
     
    public void DeleteCustomer(String pNIC){
        this.CustomerNIC=pNIC;
        con=DBAccess.DBconnect.connect();
        
        try {
            String q="DELETE FROM Customer WHERE NIC='"+CustomerNIC+"'";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
             System.out.println(e);
        }
        
    
    }
     
     
 
    
}
