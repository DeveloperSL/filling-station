/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import JForms.MainProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Kavii
 */
public class LoginClass {
    private String NIC;
    private String Password;
    private String Role;
    
    private boolean Verified=false;
    private boolean ValidNIC=false;
    
    public Connection con=null;
    public PreparedStatement pst = null;
    //Result Set
    ResultSet rs=null;
    
    public void setLogin(String pNIC,String pPassword,String pRole){
        this.NIC=pNIC;
        this.Password=pPassword;
        this.Role=pRole;
  
        con=DBAccess.DBconnect.connect();
      
        try {
            String q="INSERT INTO login(NIC,Password,Role) VALUES('"+this.NIC+"','"+this.Password+"','"+this.Role+"')";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
    
    }
    //Verify Login
    public boolean getLogin(String pNIC,String pPassword){
        con=DBAccess.DBconnect.connect();
        
        try {
            String _q="SELECT NIC FROM login ";
            pst=con.prepareStatement(_q);
            rs=pst.executeQuery();
            
            while(rs.next()){
                if((rs.getString(1)).equals(pNIC)){
                    ValidNIC=true;
                }
               
            }
      
        } catch (SQLException e) {
            System.out.println(e);
        }
     
        if(ValidNIC==true){
            try {
                    String q="SELECT Password FROM login WHERE NIC='"+pNIC+"'  ";
                    pst=con.prepareStatement(q);
                    rs=pst.executeQuery();
            
                    if(rs.next()){
                        this.Password=rs.getString(1);
                    }
                    
                    if(this.Password.equals(pPassword)){
                        this.Verified=true;
                        return this.Verified;
                    }
                    else{
                        return this.Verified;
                    }
           
            } catch (SQLException e) {
                    System.out.println(e);
                    
            }
        
        }
        
        return this.Verified;
        
        
       
    }
    
    //Delete Login
    public void DeleteLogin(String pEmpID,String pRole){
        
        con=DBAccess.DBconnect.connect();
        
        if(pRole.equals("Manager")){
            try {
                String q="SELECT NIC FROM manager WHERE EmployeeID='"+pEmpID+"'  ";
                pst=con.prepareStatement(q);
                rs=pst.executeQuery();
            
                if(rs.next()){
                    this.NIC=rs.getString(1);
                }
                
           
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        else if(pRole.equals("Sales Assistant")){
            try {
                String q="SELECT NIC FROM salesassistant WHERE EmployeeID='"+pEmpID+"'  ";
                pst=con.prepareStatement(q);
                rs=pst.executeQuery();
            
                if(rs.next()){
                    this.NIC=rs.getString(1);
                }
            
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        
        try {
            String q="DELETE FROM login WHERE NIC='"+this.NIC+"'";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
         
    }
    
   //Set LoggedIn Account Details 
   public ResultSet getLogginEmployeeDetails(){
       this.NIC=MainProfile.getLoginNICDetails();
     
       con=DBAccess.DBconnect.connect();
       
       try {
           String q="SELECT NIC FROM manager";
           pst=con.prepareStatement(q);
           rs=pst.executeQuery();
           
           while(rs.next()){
                if((rs.getString(1)).equals(this.NIC)){
                   q="SELECT * FROM manager WHERE NIC='"+this.NIC+"'";
                   pst=con.prepareStatement(q);
                   rs=pst.executeQuery();
                   return rs;
                }
               
           }
           
           q="SELECT NIC FROM salesassistant";
           pst=con.prepareStatement(q);
           rs=pst.executeQuery();
           
           while(rs.next()){
                if((rs.getString(1)).equals(this.NIC)){
                   q="SELECT * FROM salesassistant WHERE NIC='"+this.NIC+"'";
                   pst=con.prepareStatement(q);
                   rs=pst.executeQuery();
                   return rs;
                }
               
           }
           
           
       } catch (SQLException e) {
           System.out.println(e);
       }
       
       
       
       return rs;
   }
   
   //Update Password
   public void UpdatePassword(String pNIC,String pNewPassword){
       this.NIC=pNIC;
       this.Password=pNewPassword;
       con=DBAccess.DBconnect.connect();
        
       try {
            String q="UPDATE login SET Password = '"+this.Password+"' WHERE NIC='"+this.NIC+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
       } catch (SQLException e) {
            System.out.println(e);
       }
   
   }
    

   
   
}
