/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Kavii
 */
public class MiscellaneousItemClass extends ProductClass{
    private int CurrentNoItems;
    
    //Result Set
    ResultSet rs=null;
   
    
    public void SetMiscellaneousItem(String pProductName,double pUnitPrice,int pCurrentNoItems){
         
       super.ProductName=pProductName;
       super.SalesPrice=pUnitPrice;
       this.CurrentNoItems=pCurrentNoItems;
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="INSERT INTO miscellaneousitem(Name,Unit_Price,Current_No_Items) VALUES('"+ProductName+"','"+SalesPrice+"','"+CurrentNoItems+"')";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }   
    }
    //Get MiscellaneousItem Names
    
    public ResultSet GetMiscellaneousItemName(){
         
       con=DBAccess.DBconnect.connect();
      
         try {
            String sql="SELECT Name FROM miscellaneousitem";
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    
    //Update MiscellaneousItem
    
    public void UpdateMiscellaneousItem(String pProductID,String pProductName,double pSalesPrice){
       super.ProductID=pProductID;  
       super.ProductName=pProductName;
       super.SalesPrice=pSalesPrice;
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="UPDATE miscellaneousitem SET Name = '"+ProductName+"',Unit_Price='"+SalesPrice+"' WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    
    //Get MiscellaneousItemID
    
    public String GetMiscellaneousItemID(String pProductName){
        this.ProductName=pProductName;
        
        con=DBAccess.DBconnect.connect();
      
        try {
            String q="SELECT ProductID FROM miscellaneousitem WHERE Name='"+ProductName+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.ProductID=rs.getString(1);
            }
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.ProductID;
        
    }
    
    //Update MiscellaneousItem Quantity
    
    public void UpdateMiscellaneousItemQuantity(int pProductID,int pNewQuantity){
       super.ProductID=Integer.toString(pProductID);  
       int NewQuantity=pNewQuantity;
      
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="SELECT Current_No_Items FROM miscellaneousitem WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.CurrentNoItems=rs.getInt(1);
            }
            this.CurrentNoItems=this.CurrentNoItems+NewQuantity;
            
            
            q="UPDATE miscellaneousitem SET  Current_No_Items = '"+CurrentNoItems+"' WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    //Delete MiscellaneousItem
    public void DeleteMiscellaneousItem(String pMiscellaneousItemID){
        super.ProductID=pMiscellaneousItemID;
        con=DBAccess.DBconnect.connect();
        
        
        try {
            String q="DELETE FROM miscellaneousitem WHERE ProductID='"+ProductID+"'";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
    
    }
    



    
    
}
