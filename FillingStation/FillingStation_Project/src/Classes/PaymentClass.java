/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.Date;

/**
 *
 * @author Kavii
 */
public class PaymentClass {
    protected String PaymentID;
    protected String PaymentType;
    protected double Amount;
    protected Date PurchaseDate;
    
}
class CreditClass extends PaymentClass{
    private Date DueDate;
    public CreditClass() {
        super.PaymentType="Credit";
    }
    
}
class BankCardClass extends PaymentClass {

    public BankCardClass() {
        super.PaymentType="BankCard";
    }
    
}

class CashClass extends PaymentClass{

    public CashClass() {
        super.PaymentType="Cash";
    }
    

}
