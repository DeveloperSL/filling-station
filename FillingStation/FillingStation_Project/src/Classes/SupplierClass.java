/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Kavii
 */
public class SupplierClass {
    private String SupplierID;
    private String SupplierName;
    private String SupplierTelephone;
    private String SupplierAddress;
    
    public Connection con=null;
    public PreparedStatement pst = null;
    
    //Result Set
    ResultSet rs=null;
    
    public void SetSupplier(String pSupplierName,String pSupplierTelephone,String pSupplierAddress){
         
       this.SupplierName=pSupplierName;
       this.SupplierTelephone=pSupplierTelephone;
       this.SupplierAddress=pSupplierAddress;
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="INSERT INTO supplier(Name,Telephone,Address) VALUES('"+SupplierName+"','"+SupplierTelephone+"','"+SupplierAddress+"')";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    //Get Supplier Names
    
    public ResultSet GetSupplierName(){
         
       con=DBAccess.DBconnect.connect();
      
         try {
            String sql="SELECT Name FROM supplier";
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
    }
    
    
    
    
    //Update Supplier
    
    public void UpdateSupplier(String pSupplierID,String pSupplierName,String pSupplierTelephone,String pSupplierAddress){
       this.SupplierID=pSupplierID;  
       this.SupplierName=pSupplierName;
       this.SupplierTelephone=pSupplierTelephone;
       this.SupplierAddress=pSupplierAddress;
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="UPDATE supplier SET Name = '"+SupplierName+"',Telephone='"+SupplierTelephone+"',Address='"+SupplierAddress+"' WHERE SupplierID='"+SupplierID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    //Get SupplierID
    public String GetSupplierID(String pSupplierName){
        this.SupplierName=pSupplierName;
        
        con=DBAccess.DBconnect.connect();
      
        try {
            String q="SELECT SupplierID FROM supplier WHERE Name='"+SupplierName+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.SupplierID=rs.getString(1);
            }
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.SupplierID;
        
    }
    
    
    //Delete Supplier
    public void DeleteSupplier(String pSupplierID){
        this.SupplierID=pSupplierID;
        con=DBAccess.DBconnect.connect();
        
        
        try {
            String q="DELETE FROM supplier WHERE SupplierID='"+SupplierID+"'";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
    
    }
    
    
}
